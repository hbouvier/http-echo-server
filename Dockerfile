FROM golang:1.10-alpine as build

WORKDIR /go/src/http-echo-server

COPY server.go .
RUN go get -d -v ./... && \
    go install -v ./...

# -----------------------------------------------

FROM alpine

WORKDIR /app

COPY --from=build /go/bin/http-echo-server /app/
COPY public /app/public
COPY templates /app/templates

ENV PORT=3000

EXPOSE 3000

CMD /app/http-echo-server
